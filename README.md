This is a small [Pascal](https://en.wikipedia.org/wiki/Pascal_%28programming_language%29) interpreter written in [Perl 6](https://perl6.org).

It is based on the series of blog posts titled *Let's Build A Simple Interpreter* from Ruslan. The first part can be read [here](https://ruslanspivak.com/lsbasi-part1/).

You can launch tests with `prove -e perl6 -lrv t/`.
