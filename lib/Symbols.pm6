use AST;


enum SymbolType is export <INTEGER REAL>;


class Symbol {
    has Str $.name;
    has $.type;
}


class BuiltinTypeSymbol is Symbol is export {
}


class VarSymbol is Symbol is export {
}


class SymbolTable is export {
    has %!symbols{Str => Symbol};

    submethod BUILD() {
        self.init();
    }

    method init() {
        self.define(BuiltinTypeSymbol.new(name => 'INTEGER'));
        self.define(BuiltinTypeSymbol.new(name => 'REAL'));
    }

    method define(Symbol $symbol) {
        %!symbols{$symbol.name} = $symbol;
    }

    method lookup(Str $name) returns Symbol {
        return %!symbols{$name};
    }
}


class SymbolTableBuilder is NodeVisitor {
    has SymbolTable $!symtab;

    submethod BUILD() {
        $!symtab = SymbolTable.new();
    }

    multi submethod visit(Block $node) {
        for $node.declarations -> $declaration {
            self.visit($declaration);
        }

        self.visit($node.compound-statement);
    }

    multi submethod visit(Program $node) {
        self.visit($node.block);
    }

    multi submethod visit(BinOp $node) {
        self.visit($node.right);
        self.visit($node.left);
    }

    multi submethod visit(Number $node) {
    }

    multi submethod visit(UnaryOp $node) {
        self.visit($node.expr);
    }

    multi submethod visit(Compound $node) {
        for $node.children -> $child {
            self.visit($child);
        }
    }

    multi submethod visit(NoOp $node) {
    }

    multi submethod visit(VarDecl $node) {
        my $type-name = $node.type-node.value;
        my $type-symbol = $!symtab.lookup($type-name);
        my $var-name = $node.var-node.value;
        my $var-symbol = VarSymbol.new(name => $var-name, type => $type-symbol);
        $!symtab.define($var-symbol);
    }

    multi submethod visit(Assign $node) {
        my $var-name = $node.left.value;
        my $var-symbol = $!symtab.lookup($var-name);
        if !$var-symbol {
            die "$var-name is not defined";
        }

        self.visit($node.right);
    }

    multi submethod visit(Var $node) {
        my $var-name = $node.value;
        my $var-symbol = $!symtab.lookup($var-name);
        if !$var-symbol {
            die "$var-name is not defined";
        }
    }

    multi submethod visit(ProcedureDecl $node) {
    }
}
