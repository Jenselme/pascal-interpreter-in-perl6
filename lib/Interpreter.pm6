unit module Interpreter;
use AST;
use Lexer;
use Parser;
use Token;
use Symbols;


class Interpreter is NodeVisitor {
    has Program $!tree;
    has %!GLOBAL-SCOPE;

    submethod BUILD(:$tree) {
        $!tree = $tree;
    }

    multi submethod visit(Program $node) {
        self.visit($node.block);
    }

    multi submethod visit(Block $node) {
        for $node.declarations -> $declaration {
            self.visit($declaration);
        }

        self.visit($node.compound-statement);
    }

    multi submethod visit(VarDecl $node) {
        # Do nothing.
    }

    multi submethod visit(Type $node) {
        # Do nothing.
    }

    multi submethod visit(Number $node) {
        return $node.value;
    }

    multi submethod visit(BinOp $node) {
        given $node.op.type {
            when ADD {
                return self.visit($node.left) + self.visit($node.right);
            }
            when SUB {
                return self.visit($node.left) - self.visit($node.right);
            }
            when MULTIPLY {
                return self.visit($node.left) * self.visit($node.right);
            }
            when INTEGER_DIV {
                return Int(self.visit($node.left) / self.visit($node.right));
            }
            when FLOAT_DIV {
                return self.visit($node.left) / self.visit($node.right);
            }
        }
    }

    multi submethod visit(UnaryOp $node) {
        given $node.op.type {
            when ADD {
                return self.visit($node.expr);
            }
            when SUB {
                return -self.visit($node.expr);
            }
            default {
                die 'Unsuported token type for UnaryOp';
            }
        }
    }

    multi submethod visit(Compound $node) {
        for $node.children -> $child {
            self.visit($child);
        }
    }

    multi submethod visit(NoOp $node) {
    }

    multi submethod visit(Assign $node) {
        my $var-name = $node.left.value.uc();
        %!GLOBAL-SCOPE{$var-name} = self.visit($node.right);
    }

    multi submethod visit(Var $node) {
        my $var-name = $node.value.uc();
        my $val = %!GLOBAL-SCOPE{$var-name};

        return $val;
    }

    multi submethod visit(ProcedureDecl $node) {
    }

    method eval() {
        return self.visit($!tree);
    }

    method eval-return-state() {
        self.eval();

        my $result = '';

        for %!GLOBAL-SCOPE.keys.sort -> $var-name {
            my $value = %!GLOBAL-SCOPE{$var-name};
            $result ~= "$var-name => $value, ";
        }

        return $result.chop(2);
    }
}


sub create_interpreter($text) is export {
    my $lexer = Lexer.new(text => $text);
    my $parser = Parser.new(lexer => $lexer);

    # Check that variable are defined before they are used.
    my $tree = $parser.parse();
    my $symtab-builder = SymbolTableBuilder.new();
    $symtab-builder.visit($tree);

    return Interpreter.new(tree => $tree);
}
