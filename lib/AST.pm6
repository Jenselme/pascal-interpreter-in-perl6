use Token;


class NodeVisitor is export {
    multi method visit(AST $node) {
        my $info = $node.gist;
        die "Unsupported node type $info";
    }
}


class AST is export {
}


class BinOp is AST is export {
    has Token $.op;
    has AST $.left;
    has AST $.right;

    submethod BUILD(:$left, :$op, :$right) {
        $!left = $left;
        $!op = $op;
        $!right = $right;
    }
}


class UnaryOp is AST is export {
    has Token $.op;
    has AST $.expr;

    submethod BUILD(:$op, :$expr) {
        $!op = $op;
        $!expr = $expr;
    }
}


class Number is AST is export {
    has Token $!token;
    has Numeric $.value;

    submethod BUILD(:$token) {
        $!token = $token;
        $!value = $token.value;
    }
}


class Compound is AST is export {
    # Represents a "BEGIN … END" block
    has @.children;

    submethod BUILD() {
        @!children = ();
    }

    method push(AST $node) {
        @!children.push($node);
    }
}


class Assign is AST is export {
    has Token $!token;
    has AST $.left;
    has AST $.right;

    submethod BUILD(:$left, :$op, :$right) {
        $!token = $op;
        $!left = $left;
        $!right = $right;
    }
}


class Var is AST is export {
    has Token $!token;
    has $.value;

    submethod BUILD(:$token) {
        $!token = $token;
        $!value = $token.value;
    }
}


class NoOp is AST is export {
}


class Program is AST is export {
    has Str $.name;
    has AST $.block;

    submethod BUILD(:$name, :$block) {
        $!name = $name;
        $!block = $block;
    }
}


class Block is AST is export {
    has @.declarations;
    has AST $.compound-statement;

    submethod BUILD(:@declarations, :$compound-statement) {
        @!declarations = @declarations;
        $!compound-statement = $compound-statement;
    }
}


class VarDecl is AST is export {
    has AST $.var-node;
    has AST $.type-node;

    submethod BUILD(:$var-node, :$type-node) {
        $!var-node = $var-node;
        $!type-node = $type-node;
    }
}


class Type is AST is export {
    has Token $!token;
    has $.value;

    submethod BUILD(:$token) {
        $!token = $token;
        $!value = $token.value;
    }
}


class ProcedureDecl is AST is export {
    has Str $.name;
    has Block $.block;

    submethod BUILD(:$name, :$block) {
        $!name = $name;
        $!block = $block;
    }
}
