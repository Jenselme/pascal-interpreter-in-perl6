use Token;


my %RESERVED-KEYWORDS = 'BEGIN' => Token.new(type => KEYWORD, value => 'BEGIN'),
                        'END' => Token.new(type => KEYWORD, value => 'END'),
                        'INTEGER' => Token.new(type => KEYWORD, value => 'INTEGER'),
                        'PROCEDURE' => Token.new(type => KEYWORD, value => 'PROCEDURE'),
                        'PROGRAM' => Token.new(type => KEYWORD, value => 'PROGRAM'),
                        'REAL' => Token.new(type => KEYWORD, value => 'REAL'),
                        'VAR' => Token.new(type => KEYWORD, value => 'VAR');


class Lexer is export {
    has Str $!text;
    has Int $!position;
    has Str $!current-char;

    submethod BUILD(:$text) {
        $!text = $text;
        $!position = 0;
        $!current-char = self.get-current-char();
    }

    method get-current-char() {
        return $!text.split('').grep(* ne '')[$!position];
    }

    method advance($step = 1) {
        $!position += $step;
        if $!position > $!text.chars -1 {
            $!current-char = Nil;
        } else {
            $!current-char = self.get-current-char();
        }
    }

    method skip-whitespaces() {
        while so $!current-char and ($!current-char eq ' ' or $!current-char eq "\n" or $!current-char eq "\r") {
            self.advance();
        }
    }

    method skip-comment() {
        while $!current-char ne '}' {
            self.advance();
        }
        # The closing curly brace.
        self.advance();
    }

    method number() {
        # Return a (multidigit) integer or float consumed from the input.
        my $number = '';
        while so $!current-char and $!current-char ~~ /\d/ {
            $number = $number ~ $!current-char;
            self.advance();
        }

        if $!current-char eq '.' {
            $number = $number ~ '.';
            self.advance();
            while so $!current-char and $!current-char ~~ /\d/ {
                $number = $number ~ $!current-char;
                self.advance();
            }

            return Token.new(type => REAL_CONST, value => Rat($number));
        } else {
            return Token.new(type => INTEGER_CONST, value => Int($number));
        }
    }

    method get-next-token() {
        while so $!current-char {
            given $!current-char {
                when ' '|"\n"|"\r" {
                    self.skip-whitespaces();
                }
                when /\d/ {
                    return self.number();
                }
                when /\w/ {
                    if $!current-char.uc() eq 'D' and self.peek().uc() eq 'I' and self.peek(2).uc() eq 'V' {
                        self.advance(3);
                        return Token.new(type => INTEGER_DIV, value => 'DIV');
                    }

                    return self.id();
                }
                when ':' {
                    if self.peek() eq '=' {
                        self.advance(2);
                        return Token.new(type => ASSIGN, value => ':=');
                    } else {
                        self.advance();
                        return Token.new(type => COLON, value => ':');
                    }
                }
                when ',' {
                    self.advance();
                    return Token.new(type => COMMA, value => ',');
                }
                when ';' {
                    self.advance();
                    return Token.new(type => SEMI, value => ';');
                }
                when '.' {
                    self.advance();
                    return Token.new(type => DOT, value => '.');
                }
                when '*' {
                    self.advance();
                    return Token.new(type => MULTIPLY, value => '*');
                }
                when '+' {
                    self.advance();
                    return Token.new(type => ADD, value => '+');
                }
                when '-' {
                    self.advance();
                    return Token.new(type => SUB, value => '-');
                }
                when '/' {
                    self.advance();
                    return Token.new(type => FLOAT_DIV, value => '/');
                }
                when '(' {
                    self.advance();
                    return Token.new(type => LPAREN, value => '(');
                }
                when ')' {
                    self.advance();
                    return Token.new(type => RPAREN, value => ')');
                }
                when '{' {
                    self.skip-comment();
                }
                default {
                    self.error();
                }
            }
        }

        return Token.new(type => EOF, value => '');
    }

    method peek($step = 1) {
        my $peek-pos = $!position + $step;
        if $peek-pos > $!text.chars - 1 {
            return Nil;
        } else {
            return $!text.split('').grep(* ne '')[$peek-pos];
        }
    }

    method id() {
        my $result = '';
        while so $!current-char and $!current-char ~~ /\w/ {
            $result = $result ~ $!current-char;
            self.advance();
        }
        $result = $result.uc();

        return %RESERVED-KEYWORDS{$result} || Token.new(type => ID, value => $result);
    }

    method error() {
        die "Invalid character parsing $!current-char ($!position)";
    }
}
