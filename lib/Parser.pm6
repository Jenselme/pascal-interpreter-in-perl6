use AST;
use Lexer;
use Token;


class Parser is export {
    has Token $!current-token;
    has Lexer $!lexer;
    has Int $!level;

    submethod BUILD(:$lexer) {
        $!lexer = $lexer;
        $!current-token = $!lexer.get-next-token();
        $!level = 0;
    }

    method eat($token-type) {
        if $!current-token.type == $token-type {
            $!current-token = $!lexer.get-next-token();
        } else {
            self.syntax-error();
        }
    }

    method eat-keyword($keyword-name) {
        if $!current-token.type != KEYWORD {
            self.syntax-error();
        }

        if $!current-token.value eq $keyword-name {
            $!current-token = $!lexer.get-next-token();
        } else {
            self.syntax-error();
        }
    }

    method syntax-error() {
        my $value = $!current-token.value;
        my $type = $!current-token.type;
        die "Invalid syntax parsing $value ($type)";
    }

    method program() {
        # program : PROGRAM variable SEMI block DOT
        self.eat-keyword('PROGRAM');
        my $var-node = self.variable();
        my $prog-name = $var-node.value;
        self.eat(SEMI);
        my $block-node = self.block();
        my $program-node = Program.new(name => $prog-name, block => $block-node);
        self.eat(DOT);

        return $program-node;
    }

    method block() {
        # block : declarations compound_statement
        my $declaration-nodes = self.declarations();
        my $compound-statement-node = self.compound-statement();
        my $node = Block.new(declarations => $declaration-nodes, compound-statement => $compound-statement-node);

        return $node;
    }

    method declarations() {
        # declarations : VAR (variable_declaration SEMI)+
        #              | (PROCEDURE ID SEMI block SEMI)*
        #              | empty
        my @declarations = ();
        if $!current-token.type == KEYWORD and $!current-token.value eq 'VAR' {
            self.eat-keyword('VAR');
            while $!current-token.type == ID {
                my @var-decl = self.variable-declaration();
                @declarations.append(@var-decl);
                self.eat(SEMI);
            }
        }

        while $!current-token.type == KEYWORD and $!current-token.value eq 'PROCEDURE' {
            self.eat-keyword('PROCEDURE');
            my $proc-name = $!current-token.value;
            self.eat(ID);
            self.eat(SEMI);
            my $block-node = self.block();
            my $proc-decl = ProcedureDecl.new(name => $proc-name, block => $block-node);
            @declarations.append($proc-decl);
            self.eat(SEMI);
        }

        return @declarations;
    }

    method variable-declaration() {
        # variable_declaration : ID (COMMA ID)* COLON type_spec
        my @var-nodes = (Var.new(token => $!current-token));
        self.eat(ID);
        while $!current-token.type == COMMA {
            self.eat(COMMA);
            @var-nodes.push(Var.new(token => $!current-token));
            self.eat(ID);
        }

        self.eat(COLON);

        my $type-node = self.type-spec();
        my @var-declarations = ();
        for @var-nodes -> $node {
            @var-declarations.push(VarDecl.new(var-node => $node, type-node => $type-node));
        }

        return @var-declarations;
    }

    method type-spec() {
        # type_spec : INTEGER
        #           | REAL
        my $token = $!current-token;
        given $!current-token.value {
            when 'INTEGER' {
                self.eat-keyword('INTEGER');
            }
            when 'REAL' {
                self.eat-keyword('REAL');
            }
            default {
                my $value = $!current-token.value;
                die "Unknown type '$value'";
            }
        }

        my $node = Type.new(token => $token);
        return $node;
    }

    method compound-statement() {
        # compound_statement: BEGIN statement_list END
        self.eat-keyword('BEGIN');
        my @nodes = self.statement-list();
        self.eat-keyword('END');

        my $root = Compound.new();
        for @nodes -> $node {
            $root.push($node);
        }

        return $root;
    }

    method statement-list() {
        # statement_list : statement
        #                | statement SEMI statement_list
        my $statement = self.statement();
        my @results = ($statement);

        while $!current-token.type == SEMI {
            self.eat(SEMI);
            @results.push(self.statement());
        }

        if $!current-token.type == ID {
            self.syntax-error();
        }

        return @results;
    }

    method statement() {
        # statement : compound_statement
        #           | assignment_statement
        #           | empty
        my $node;
        given $!current-token.type {
            when KEYWORD {
                if $!current-token.value eq 'BEGIN' {
                    $node = self.compound-statement();
                } else {
                    $node = self.empty();
                }
            }
            when ID {
                $node = self.assignment-statement();
            }
            default {
                $node = self.empty();
            }
        }

        return $node;
    }

    method assignment-statement() {
        # assignment_statement : variable ASSIGN expr
        my $left = self.variable();
        my $token = $!current-token;
        self.eat(ASSIGN);
        my $right = self.expr();
        my $node = Assign.new(left => $left, op => $token, right => $right);

        return $node;
    }

    method variable() {
        # variable: ID
        my $node = Var.new(token => $!current-token);
        self.eat(ID);

        return $node;
    }

    method empty() {
        return NoOp.new();
    }

    method factor() {
        # factor : PLUS factor
        #        | MINUS factor
        #        | INTEGER_CONST
        #        | REAL_CONST
        #        | LPAREN expr RPAREN
        #        | variable
        my $token = $!current-token;
        given $token.type {
            when LPAREN {
                self.eat(LPAREN);
                $!level++;
                my $node = self.expr();
                self.eat(RPAREN);
                $!level--;

                return $node;
            }
            when INTEGER_CONST {
                self.eat(INTEGER_CONST);
                # Make sure error is thrown in cases like 1 + 1)
                self.check-paren-balance();

                return Number.new(token => $token);
            }
            when REAL_CONST {
                self.eat(REAL_CONST);
                # Make sure error is thrown in cases like 1 + 1)
                self.check-paren-balance();

                return Number.new(token => $token);
            }
            when SUB {
                self.eat(SUB);
                return UnaryOp.new(op => $token, expr => self.factor());
            }
            when ADD {
                self.eat(ADD);
                return UnaryOp.new(op => $token, expr => self.factor());
            }
            default {
                return self.variable();
            }
        }
    }

    method check-paren-balance() {
        if $!current-token.type == RPAREN and $!level == 0 {
            self.syntax-error();
        }
    }

    method term() {
        # term : factor ((MUL | INTEGER_DIV | FLOAT_DIV) factor)*
        my $node = self.factor();

        while $!current-token.type ∈ (MULTIPLY, INTEGER_DIV, FLOAT_DIV) {
            my $token = $!current-token;
            given $token.type {
                when MULTIPLY {
                    self.eat(MULTIPLY);
                }
                when INTEGER_DIV {
                    self.eat(INTEGER_DIV);
                }
                when FLOAT_DIV {
                    self.eat(FLOAT_DIV);
                }
            }

            $node = BinOp.new(left => $node, op => $token, right => self.factor());
        }

        # Make sure error is thrown in cases like 1 + 1)
        self.check-paren-balance();

        return $node;
    }

    method expr() {
        my $node = self.term();

        while $!current-token.type ∈ (ADD, SUB) {
            my $token = $!current-token;
            if $token.type == ADD {
                self.eat(ADD);
            } elsif $token.type == SUB {
                self.eat(SUB);
            }

            $node = BinOp.new(left => $node, op => $token, right => self.term());
        }

        # Make sure error is thrown in cases like 1 + 1)
        self.check-paren-balance();

        return $node;
    }

    method parse() {
        my $node = self.program();
        if $!current-token.type != EOF {
            self.error();
        }

        return $node;
    }
}
