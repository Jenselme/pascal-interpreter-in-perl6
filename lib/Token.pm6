enum Terminal is export <INTEGER_CONST REAL_CONST COLON COMMA MULTIPLY INTEGER_DIV FLOAT_DIV ADD SUB LPAREN RPAREN EOF KEYWORD ID DOT ASSIGN SEMI>;


class Token is export {
    has Terminal $.type;
    has $.value;
}
