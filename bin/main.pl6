use lib 'lib';
use Lexer;
use Parser;
use Interpreter;


sub main() {
    print 'calc> ';

    for $*IN.lines() -> $line {
        my $interpreter = create_interpreter($line);
        say $interpreter.eval();
        print 'calc> ';
    }
}

main();
