use v6;
use Test;
use lib 'lib';
use Interpreter;


is create_interpreter('PROGRAM test; BEGIN END.').eval-return-state(), {};
is create_interpreter('PROGRAM test;
BEGIN
END.').eval-return-state(), {};
is create_interpreter('PROGRAM test;
VAR
BEGIN
END.').eval-return-state(), {};
is create_interpreter('PROGRAM test;
BEGIN {comment}
END.').eval-return-state(), {};
is create_interpreter('PROGRAM test;
VAR
    test : INTEGER;
BEGIN
END.').eval-return-state(), {};
is create_interpreter('PROGRAM test;
VAR
    test1, test2 : INTEGER;
BEGIN
END.').eval-return-state(), {};
is create_interpreter('PROGRAM Part10;
VAR
   number     : INTEGER;
   a, b, c, x : INTEGER;
   y          : REAL;

BEGIN {Part10}
   BEGIN
      number := 2;
      a := number;
      b := 10 * a + 10 * number DIV 4;
      c := a - - b
   END;
   x := 11;
   y := 20 / 7 + 3.14;
END. {Part10}').eval-return-state(), 'A => 2, B => 25, C => 27, NUMBER => 2, X => 11, Y => 5.997143';
is create_interpreter('PRoGRaM Part10;
VAr
   number     : INTEGEr;
   a, b, c, x : iNTEGER;
   y          : rEAL;

BEGIN {Part10}
   bEGIN
      number := 2;
      a := Number;
      b := 10 * a + 10 * numbER DiV 4;
      c := a - - b
   END;
   x := 11;
   y := 20 / 7 + 3.14;
EnD. {Part10}').eval-return-state(), 'A => 2, B => 25, C => 27, NUMBER => 2, X => 11, Y => 5.997143';
is create_interpreter('PROGRAM Part11;
VAR
   number : INTEGER;
   a, b   : INTEGER;
   y      : REAL;
BEGIN {Part11}
   number := 2;
   a := number ;
   b := 10 * a + 10 * number DIV 4;
   y := 20 / 7 + 3.14
END.  {Part11}').eval-return-state(), 'A => 2, B => 25, NUMBER => 2, Y => 5.997143';
is create_interpreter('PROGRAM Part12;
VAR
   a : INTEGER;

PROCEDURE P1;
VAR
   a : REAL;
   k : INTEGER;

   PROCEDURE P2;
   VAR
      a, z : INTEGER;
   BEGIN {P2}
      z := 777;
   END;  {P2}

BEGIN {P1}

END;  {P1}

BEGIN {Part12}
   a := 10;
END. {Part12}').eval-return-state(), 'A => 10';



dies-ok { create_interpreter('').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN END').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('a').eval() }, 'Invalid character';
dies-ok { create_interpreter('BEGIN ( 1 + 1) END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN a := ( 1 + 1 END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN a := 1 / 1 END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN a := 1 + 1) END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN a := 2 * 3) END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN a := (1 + 1) * (2 * (6 -2) END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN a := 1 + 1) * (2 * (6 -2)) END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN a := (1 + 1) * (2 * 6 -2)) END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('BEGIN a := (1 + 1) * (2 * 6 -2)) + 1 END.').eval() }, 'Invalid syntax';
dies-ok { create_interpreter('PROGRAM Part11;
VAR
   number : INTEGER;
   a, b   : INTEGER;
BEGIN {Part11}
   number := 2;
   a := number ;
   b := 10 * a + 10 * number DIV 4;
   y := 20 / 7 + 3.14
END.  {Part11}') }, 'y not declared before assignemnt';
dies-ok { create_interpreter('PROGRAM Part11;
VAR
   number : INTEGER;
   a, b   : INTEGER;
BEGIN {Part11}
   number := 2 + y;
   a := number;
   b := 10 * a + 10 * number DIV 4;
END.  {Part11}') }, 'y not declared before usage';


done-testing;
